#
# Makefile ESQUELETO
#
# OBRIGATÓRIO ter uma regra "all" para geração da biblioteca e de uma
# regra "clean" para remover todos os objetos gerados.
#
# NECESSARIO adaptar este esqueleto de makefile para suas necessidades.
# 

CC=gcc
LIB_DIR=./lib
INC_DIR=./include
BIN_DIR=./bin
SRC_DIR=./src

all: queue table context scheduler sthread debug
	ar crs $(LIB_DIR)/libsthread.a $(BIN_DIR)/*.o

queue: $(SRC_DIR)/queue.c $(INC_DIR)/queue.h $(INC_DIR)/sdata.h
	$(CC) -Wall -ggdb -c $(SRC_DIR)/queue.c -o $(BIN_DIR)/queue.o
	
table: $(SRC_DIR)/table.c $(INC_DIR)/table.h $(INC_DIR)/sdata.h
	$(CC) -Wall -ggdb -c $(SRC_DIR)/table.c -o $(BIN_DIR)/table.o

context: $(SRC_DIR)/context.c $(INC_DIR)/context.h  $(INC_DIR)/sdata.h
	$(CC) -Wall -ggdb -c $(SRC_DIR)/context.c -o $(BIN_DIR)/context.o

scheduler: $(SRC_DIR)/scheduler.c $(INC_DIR)/scheduler.h  $(INC_DIR)/sdata.h
	$(CC) -Wall -ggdb -c $(SRC_DIR)/scheduler.c -o $(BIN_DIR)/scheduler.o
	
sthread: $(SRC_DIR)/sthread.c $(INC_DIR)/global.h  $(INC_DIR)/sdata.h
	$(CC) -Wall -ggdb -c $(SRC_DIR)/sthread.c -o $(BIN_DIR)/sthread.o
	
debug: $(SRC_DIR)/debug.c $(INC_DIR)/debug.h  $(INC_DIR)/sdata.h
	$(CC) -Wall -ggdb -c $(SRC_DIR)/debug.c -o $(BIN_DIR)/debug.o

clean:
	rm -rf $(LIB_DIR)/*.a $(BIN_DIR)/*.o


