/*
 * context.c - Funções diversas para lidar com contextos de execução
 */

#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

#include "../include/context.h"
#include "../include/scheduler.h"
#include "../include/queue.h"
#include "../include/debug.h"
#include "../include/global.h"

ucontext_t runningContext;
s_tcb *runningThread;
s_tcb *mainThread;


/*
 * Retorna a thread que está rodando correntemente.
 * Logo que possível têm que ser criado o contexto da main().
 */
s_tcb *getRunningThread()
{
	return runningThread;
}

/*
 * Coloca a thread para executar.
 * É assumido que a thread já foi retirada das filas
 * de STATE_READYs, ou seja, que a thread tenha vindo
 * do scheduler().
 */
void dispatchThread(s_tcb *thread)
{
	runningThread = thread; // Atualiza variável global
	thread->state = STATE_RUNNING;
	PRINT("dispatchThread: dispatching thread %s\n", threadAsString(thread));
	setcontext(&thread->context);
}

/*
 * Salva o contexto atual na thread.
 */
void contextSave(s_tcb *thread)
{
	getcontext(&thread->context);
}

/*
 * Coloca a thread numa das filas de STATE_READYs.
 * Caso a prioridade da thread seja inválida, o programa
 * é abortado.
 */
void putInReadyState(s_tcb *thread)
{
	s_queue *queue;
	
	queue = getReadyQueue(thread->prio);
	if(queue == NULL) {
		printf("Erro fatal em putInReadyState: prioridade inválida\n");
		exit(-1);
	}
	thread->state = STATE_READY;
	queueAppend(queue, thread);
}

/*
 * Coloca uma thread em estado bloqueado.
 * NÃO remove das filas de STATE_READY.
 */
/*void putInBlockedState(s_tcb *thread)
{
	thread->state = STATE_BLOCKED;
	queueAppend(blockedQueue, thread);
}*/

/*
 * Não vai ser preciso. Pois a thread em execução
 * nunca está nas filas de STATE_READYs.
 */
/*void removeFromReadyState(s_tcb *thread)
{
	thread->state = STATE_UNDEF;
	queue = getReadyQueue(thread->prio);
	queueAppend(queue, thread);
}*/
