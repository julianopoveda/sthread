/*
 * table.c - Implementação do TAD
 */

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>

#include "../include/sdata.h"
#include "../include/table.h"

s_table *threadTable;

/*
 * Alloca uma tabela de threads com 'size' linhas.
 */
s_table *tableAlloc(int size)
{
	int i;
	
	if(size <= 0) {
		printf("Erro fatal: tamanho inválido ao alocar tabela de threads.\n");
		exit(-1);
	}
	
	s_table *table = (s_table *) malloc(sizeof(s_table));
	table->row = (s_table_row *) malloc(size*sizeof(s_table_row));
	table->size = size;
	
	for(i = 0; i < size; i++) {
		table->row[i].isValid = false;
	}
	
	return table;
}

/*
 * Retorna a thread (ponteiro s_tcb) indexada pela tid.
 * O algoritmo de busca é linear, a tabela é percorrida linha por linha.
 * Caso tid não seja um identificador de thread válido, retorna NULL.
 */
s_tcb *tableGet(int tid)
{
	int i;
	s_table_row linha;
	
	for(i = 0; i < threadTable->size; i++) {
		linha = threadTable->row[i];
		if(linha.tid == tid && linha.isValid) {
			return linha.thread;
		}
	}
	
	return NULL;
}

/*
 * Insere uma thread na tabela.
 * Não verifica se a thread já existe.
 * Se não há mais espaço na tabela é realocada uma com o dobro
 * do tamanho.
 */
bool tableAdd(int tid, s_tcb *thread)
{
	int i;
	s_table_row *linha;
	
	// Procurando a primeira linha vazia
	for(i = 0; i < threadTable->size; i++) {
		linha = &threadTable->row[i];
		if(!linha->isValid) {
			linha->tid = tid;
			linha->thread = thread;
			linha->isValid = true;
			return true;
		}
	}
	
	// precisa realocar
	// aloca o dobro do tamanho original
	int sizeOld = threadTable->size;
	threadTable->row = (s_table_row *) realloc(threadTable->row, 2*sizeOld*sizeof(s_table_row));
	threadTable->size = 2*sizeOld;
	
	// inicializando novo espaço
	for(i = sizeOld; i < threadTable->size; i++) {
		threadTable->row[i].isValid = false;
	}
	
	// e agora a inserção
	threadTable->row[sizeOld].tid = tid;
	threadTable->row[sizeOld].thread = thread;
	threadTable->row[sizeOld].isValid = true;
	
	return true;
}

s_tcb *tableRemove(s_tcb *thread)
{
	int i;
	s_table_row *linha;
	
	for(i = 0; i < threadTable->size; i++) {
		linha = &threadTable->row[i];
		if(linha->thread == thread && linha->isValid) {
			linha->isValid = false;
			return linha->thread;
		}
	}
	
	return NULL;
}

void tablePrint()
{
	int i;
	s_table_row linha;
	
	printf("linha: VALID  TID       PTR STATE PRIO       CTX   uc_link\n");
	for(i = 0; i < threadTable->size; i++) {
		linha = threadTable->row[i];
		printf("%5d: %5d %4d %p %5d %4d %p %p\n",
			i, linha.isValid, linha.tid, linha.thread, linha.thread->state,
			linha.thread->prio, &linha.thread->context, linha.thread->context.uc_link
		);
	}
}

/*s_tcb tableRemove(int tid)
{
	int i;
	s_table_row *linha;
	
	for(i = 0; i < threadTable->size-1; i++) {
		linha = threadTable->row[i];
		if(linha->tid && linha->isValid) {
			return linha->thread;
		}
	}
	
	return NULL;
}*/
