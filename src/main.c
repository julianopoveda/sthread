#include <stdio.h>
#include "../include/sdata.h"

int main()
{
    TCB a, b, c, d;
    
    filaPrioridadeAlta  = initFila();
    filaPrioridadeMedia = initFila();
    filaPrioridadeBaixa = initFila();
    contador = 0;
    
    screate(0, NULL, NULL);
    screate(0, NULL, NULL);
    screate(0, NULL, NULL);
    screate(1, NULL, NULL);
    screate(1, NULL, NULL);
    screate(1, NULL, NULL);
    
     //teste do penultimo
    TCB *e = GetPenultimoElemento(filaPrioridadeAlta);
    if(e!=NULL){
        printf("%d",e->tid);
    }
    else
    {
        puts("fila vazia");
    }
    
    printFila(filaPrioridadeAlta, "Fila alta");
    printFila(filaPrioridadeMedia, "Fila media");
    //printFila(filaPrioridadeBaixa, "Fila baixa");
    
    return 0;
}
