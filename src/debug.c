// debug.c

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "../include/debug.h"

// Modo de debug desligado por padrão
static int debug_mode = 0;
 
void PRINT(const char* format, ...) {
	if(debug_mode != 0) {
		char *debug = "DEBUG> ";
		int tamanho = strlen(debug) + strlen(format) + 1;
		char *format_appended = (char *) malloc(tamanho);
		
		strcpy(format_appended, debug);
		strncpy(format_appended+strlen(debug), format, strlen(format));
		format_appended[tamanho-1] = '\0';
		
		va_list args;
		va_start(args, format);
		vprintf(format_appended, args);
		va_end(args);
	}
}

void turnDebugOn() {
	debug_mode = 1;
}

void turnDebugOff() {
	debug_mode = 0;
}

bool isDebugOn() {
	return debug_mode == 1;
}
