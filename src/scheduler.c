/*
 * scheduler.c
 */

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

#include "../include/sdata.h"

#include "../include/global.h"
#include "../include/queue.h"
#include "../include/context.h"
#include "../include/scheduler.h"
#include "../include/debug.h"

// Variáveis globais

s_queue *highPrioQueue;
s_queue *mediumPrioQueue;
s_queue *lowPrioQueue;

/* 
 * Decisão de projeto: não vai ter fila de bloqueados.
 * As únicas formas da thread ir para STATE_BLOCKED é por
 * meio das chamadas swait() e slock(). Toda vez que uma thread
 * terminar (via código de cleanup) ou for desbloqueada de um lock
 * a thread correspondente é recuperada pelas proprias funções.
 */

/*
 * Retorna a fila de STATE_READY de acordo com a prioridade.
 * Retorna NULL caso a prioridade seja inválida.
 */
s_queue *getReadyQueue(s_priority prio)
{
	s_queue *queue;
	switch(prio) {
		case PRIO_HIGH:
			queue = highPrioQueue;
			break;
		case PRIO_MED:
			queue = mediumPrioQueue;
			break;
		case PRIO_LOW:
			queue = lowPrioQueue;
			break;
		default:
			queue = NULL;
			break;
	}
	return queue;
}

/*
 * Escolhe uma thread para executar.
 * Sempre vai haver pelo menos uma thread (pelo menos a main()).
 * A thread é removida da correspondente fila de STATE_READY.
 */
s_tcb *scheduler()
{
	s_tcb *thread;
	
	if(!queueIsEmpty(highPrioQueue)) {
		thread = queueRemove(highPrioQueue);
	} else if(!queueIsEmpty(mediumPrioQueue)) {
		thread = queueRemove(mediumPrioQueue);
	} else {
		if(queueIsEmpty(lowPrioQueue)) {
			printf("Erro fatal em scheduler: não há nenhuma thread READY para escalonar.\n");
			exit(-1);
		}
		thread = queueRemove(lowPrioQueue);
	}
	
	thread->state = STATE_UNDEF;
	return thread;
}
