/*
 * queue.c - Implementação do TAD
 */

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>

#include "../include/sdata.h"
#include "../include/queue.h"
#include "../include/debug.h"

/*
 * Aloca uma fila e inicializa com NULL's (fila vazia).
 */
s_queue *queueAlloc()
{
	s_queue *queue = (s_queue*) malloc(sizeof(s_queue));
	queue->first = NULL;
	queue->last  = NULL;
	return queue;
}

/*
 * Adiciona um elemento (thread) na fila.
 * Para adicionar, NÃO é feita uma cópia da thread.
 * Simplismente é adicionado o ponteiro.
 * O campo 'next' da thread é alterado.
 */
bool queueAppend(s_queue *queue, s_tcb *thread)
{
	if(queueIsEmpty(queue)) {
		thread->prev = NULL;
		thread->next = NULL;
		queue->first = thread;
		queue->last  = thread;
	} else {
		thread->prev = queue->last;
		queue->last->next = thread;
		queue->last = thread;
		thread->next = NULL;
	}
	return true; // ??? OK
}

/*
 * Remove e retorna um elemento (thread) de determinada fila.
 * Se a fila estiver vazia retorna NULL.
 */
s_tcb *queueRemove(s_queue *queue)
{
	s_tcb *thread;
	
	if(queueIsEmpty(queue)) {
		return NULL;
	} else if(queue->first == queue->last) { // Última thread da fila
		thread = queue->first;
		queue->first = NULL;
		queue->last  = NULL;
	} else {
		thread = queue->first;
		thread->next->prev = NULL;
		queue->first = thread->next; // a ponta da fila é a próxima thread
	}
	thread->prev = NULL;
	
	return thread;
}

/*
 * Remove thread de fila. Não remove a thread da ponta (como em uma fila),
 * mas sim a thread igual à trd. Retorna true caso trd exista na fila.
 */
bool queueRemoveAsList(s_queue *queue, s_tcb *trd)
{
	s_tcb *thread, *aux;
	bool achou = false;
	
	for(thread = queue->first; thread != NULL; thread = thread->next) {
		if(thread == trd) {
			achou = true;
			break;
		}
	}
	
	if(achou) {
		if(thread == queue->first) {
			PRINT("queueRemoveAsList: Caso do first.\n");
			queueRemove(queue);
		} else if(thread == queue->last) {
			PRINT("queueRemoveAsList: Caso do last.\n");
			queue->last = thread->prev;
			queue->last->next = NULL;
		} else {
			PRINT("queueRemoveAsList: Caso do meio.\n");
			aux = thread->prev;
			aux->next = thread->next;
			aux = thread->next;
			aux->prev = thread->prev;
		}
		thread->prev = NULL;
		thread->next = NULL;
		trd = thread;
		return true;
	} else {
		return false;
	}
}

/*
 * Testa se a fila está vazia.
 * Por convenção testamos as duas pontas, mas se first for NULL
 * a fila está deve estar vazia também.
 */
bool queueIsEmpty(s_queue *queue)
{
	if(queue == NULL) {
		printf("Erro fatal: fila não alocada em queueIsEmpty.\n");
		exit(-1);
	}
	
	if(queue->first == NULL && queue->last == NULL) {
		return true;
	} else {
		return false;
	}
}

/*
 * Função para debug.
 * Imprime as TID dos elementos de uma fila.
 */
void queuePrint(s_queue *queue, const char *nomeFila)
{
	s_tcb *aux;
	
	if(queueIsEmpty(queue)) {
		printf("%s: vazio\n", nomeFila);
		return;
	}
	
	PRINT("%s: [first]->", nomeFila);
	for(aux = queue->first; aux != NULL; aux = aux->next)
	{
		printf(" %d", aux->tid);
	}
	printf(" <-[last]\n");
}

/*
 * Função para debug.
 * Imprime as TID dos elementos de uma fila.
 */
void queuePrintReverse(s_queue *queue, const char *nomeFila)
{
	s_tcb *aux;
	
	if(queueIsEmpty(queue)) {
		printf("%s: vazio\n", nomeFila);
		return;
	}
	
	PRINT("%s: [last]->", nomeFila);
	for(aux = queue->last; aux != NULL; aux = aux->prev)
	{
		printf(" %d", aux->tid);
	}
	printf(" <-[first]\n");
}

