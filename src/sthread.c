/*
 * sthread.c
 * Arquivo principal da biblioteca. As funções da API são implementadas aqui
 */

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

#include "../include/sthread.h"
#include "../include/sdata.h"

#include "../include/global.h"
#include "../include/context.h"
#include "../include/scheduler.h"
#include "../include/queue.h"
#include "../include/table.h" // Possuí a global threadTable
#include "../include/debug.h"


#define S_STACK_SIZE (10*SIGSTKSZ)


// São inicializados com zero pelo compilador
static bool isSThreadInitialized;
static int threadCount;

ucontext_t threadDestroyContext;


/*
 * String utilizada pela função threadAsString().
 * Ela é inicializada aqui para não ser necessário alocá-la
 * toda a vez que a função for utilizada. A string retornada
 * tem a característica de ser aproximadamente do mesmo tamanho.
 */
char *strForPrintThread[1024] = {0};

/*
 * Função que inicializa todas as estruturas de dados da biblioteca.
 * Só deve ser chamada pela screate(), as outras funções da API devem
 * verificar se 'isSThreadInitialized', e caso não, geram um erro.
 */
void initSThread()
{
	if(!isSThreadInitialized)
	{
		threadCount = 0;
		highPrioQueue   = queueAlloc();
		mediumPrioQueue = queueAlloc();
		lowPrioQueue    = queueAlloc();
		threadTable     = tableAlloc(16);
		
		// Criando o contexto para a função de cleanup de threads
		getcontext(&threadDestroyContext);
		threadDestroyContext.uc_link = NULL; // nunca é executado. Escalonador entra antes
		threadDestroyContext.uc_stack.ss_sp   = (char *) malloc(S_STACK_SIZE);
		threadDestroyContext.uc_stack.ss_size = S_STACK_SIZE;
		makecontext(&threadDestroyContext, (void (*)(void))threadDestroy, 0);
		
		PRINT("initSThread: threadDestroyContext %p\n", &threadDestroyContext);
		
		// Criando representação da thread main() na sthread
		PRINT("initSThread: criando thread main()\n");
		mainThread = threadCreate(PRIO_LOW);
		runningThread = mainThread;
		tableAdd((getRunningThread())->tid, getRunningThread()); // thread main terá tid == 1
		putInReadyState(getRunningThread());
		contextSave(getRunningThread()); // Salva "aqui" no contexto da thread main()
		
		if(!isSThreadInitialized) {
			isSThreadInitialized = true;
			PRINT("initSThread: thread main: %s\n", threadAsString(getRunningThread()));
			dispatchThread(scheduler()); // Escalona... e executa a main :)
		}
		PRINT("initSThread: inicialização concluída!\n");
	}
}

/*
 * Caso precisemos utilizar um esquema diferente
 * de geração de tid's.
 */
int genTid()
{
	return threadCount++;
}

/*
 * Usada para fins de debug.
 * Retorna uma representação da thread em um string.
 * A string retornada está em uma variável global e já é alocada pelo compilador.
 * Isso foi feito por simplicidade e para eliminar o memory leak caso fosse alocada dinâmicamente.
 * CUIDADO: cada chamada à essa função sobrepõe a variável global.
 */
char *threadAsString(s_tcb *thread)
{
	char *format = "{%p, tid %d, state: %d, prio: %d, waitedBy: %p, &context: %p, context.uc_link: %p}";
	snprintf((char *) strForPrintThread, sizeof(strForPrintThread), format,
		thread, thread->tid, thread->state, thread->prio, thread->waitedBy, &thread->context, thread->context.uc_link);
	return (char *) strForPrintThread;
}

/*
 * Cria a thread propriamente dita.
 * A maneira como threads são referênciadas pela biblioteca
 * é por meio de ponteiros. Após criada, a thread é inserida
 * em uma tabela que implementada um mapeamente de tids (que são int's)
 * para os ponteiros s_tcb.
 */
s_tcb *threadCreate(int prio)
{
	s_tcb *thread;
	
	thread = (s_tcb *) malloc(sizeof(s_tcb));
	thread->tid = genTid();
	thread->state = STATE_NEW;
	thread->prio = prio;
	thread->waitedBy = NULL;

	// É inicializado e consistido depois
	//thread->waiting = NULL;
	//thread->context = NULL
	//thread->next    = NULL;
	
	// É responsabilidade da screate()
	//putInReadyState(thread);
	
	return thread;
}

/*
 * Desaloca o contexto da thread e libera seus recursos.
 * Dentre eles, libera a thread que está esperando (que fez swait).
 * Na inicialização da biblioteca é criado um contexto para essa função.
 * Quando criando o contexto para qualquer thread, esse contexto (desta função aqui)
 * é colocado no uc_link. Assim, quando a thread terminar, essa função será executada.
 * OBS.: essa função não é executada para a "thread" main.
 */
void threadDestroy()
{
	s_tcb *thread;
	s_queue *queue;
	
	thread = getRunningThread();

	PRINT("threadDestroy: destruindo %s\n", threadAsString(thread));
	
	if(thread->waitedBy != NULL) {
		putInReadyState(thread->waitedBy);
		PRINT("threadDestroy: thread estava sendo esperada\n");
		PRINT("threadDestroy: verificando se thread que está esperando foi colocada em STATE_READY\n");
		queue = getReadyQueue(thread->waitedBy->prio);
		if(queue == NULL) {
			PRINT("threadDestroy: nop (Erro!). Thread que estava esperando não foi colocada em STATE_READY\n");
		} else {
			PRINT("threadDestroy: fila de STATE_READY que a thread que estava esperando foi colocada:\n");
			if(isDebugOn()) {
				// Para não sujar a saída do programa
				queuePrint(queue, "threadDestroy: fila ");
			}
		}
	}
	
	// Removendo do mapeamento...
	tableRemove(thread);
	
	if(thread->state == STATE_READY) {
		printf("Erro fatal em threadDestroy: thread que está terminando não poderia estar em STATE_READY\n");
		printf("%s\nbye\n", threadAsString(thread));
		exit(-1);
	}
	
	free(thread);
	
	// Segue o baile
	dispatchThread(scheduler());
}

int screate(int prio, void (*start)(void *), void *arg)
{
	s_tcb *thread;
	
	initSThread();
	
	if(getReadyQueue(prio) == NULL) {
		// Fila de STATE_READY correspondente não existe
		return -1;
	}
	
	thread = threadCreate(prio);
	
	// Quando a thread sendo criada terminar (a função terminar de executar)
	// o próximo contexto a executar será o da função responsável por fazer
	// o cleanup da thread (vide função threadDestroy()).
	getcontext(&thread->context);
	thread->context.uc_link = &threadDestroyContext;
	thread->context.uc_stack.ss_sp   = (char *) malloc(S_STACK_SIZE);
	thread->context.uc_stack.ss_size = S_STACK_SIZE;
	makecontext(&thread->context, (void (*)(void)) start, 1, arg);
	
	PRINT("screate: info da nova thread: %s\n", threadAsString(thread));
	
	tableAdd(thread->tid, thread);
	
	putInReadyState(thread);
	
	// A decisão de projeto foi não chamar o escalonador
	// ao criar uma nova thread. Então esta thread continuará
	// sua execução normal (screate não "cutuca" o escalonador).
	
	return thread->tid;
}

int swait(int tid)
{
	s_tcb *thread, *thisThread;
	bool waitDone = false;
	
	if(!isSThreadInitialized) {
		PRINT("swait: sthread não inicializada\n");
		return -1;
	}
	
	//initSThread();
	
	thread = tableGet(tid);

	if(thread == NULL) {
		PRINT("swait: thread %d não existe, inválida, ou já terminou\n", tid);
		return -1;
	}

	thisThread = getRunningThread();

	if(thread == thisThread || thread == mainThread) {
		PRINT("swait: não é permitido esperar a sí mesmo ou thread main()\n");
		return -1;
	}
	if(thread->waitedBy != NULL) {
		PRINT("swait: thread %d já está sendo esperada por outra thread\n", tid);
		return -1;
	}
	
	thisThread->state = STATE_BLOCKED;
	thread->waitedBy = thisThread;
	contextSave(thisThread);
	
	if(!waitDone) {
		waitDone = true;
		dispatchThread(scheduler());
	}
	
	return 0;
}

int syield()
{
	bool hasYielded = false;
	s_tcb *thisThread;
	
	if(!isSThreadInitialized) {
		PRINT("syield: sthread não inicializada\n");
		return -1;
	}
	
	//initSThread();
	
	// Quando esta thread voltar a executar, hasYielded vai ser true
	thisThread = getRunningThread();
	contextSave(thisThread);
	
	if(!hasYielded) {
		hasYielded = true; // É alterado mesmo com o contexto salvo.
		putInReadyState(thisThread);
		PRINT("syield: tid %d yieldou\n", thisThread->tid);
		dispatchThread(scheduler()); // Escalona e roda outra thread
		return -1; // Nunca deve ser executado
	} else {
		thisThread = getRunningThread();
		PRINT("syield: retornando chamada para tid %d\n", thisThread->tid);
		return 0;
	}
}

int smutex_init(smutex_t *mutex)
{
	// Não necessariamente um erro...
	if(!isSThreadInitialized) {
		PRINT("smutex_init: sthread não inicializada\n");
	}

	mutex->flag = false;
	mutex->who  = NULL;
	mutex->lockedQueue = queueAlloc();
	
	if(mutex == NULL || mutex->lockedQueue == NULL) {
		PRINT("smutex_init: erro ao alocar memória\n");
		return -1;
	} else {
		PRINT("smutex_init: mutex inicializado\n");
		return 0;
	}
}

int slock(smutex_t *mutex)
{
	bool insert;
	s_tcb *thisThread = getRunningThread();

	if(!isSThreadInitialized) {
		PRINT("slock: sthread não inicializada\n");
		return -1;
	}
	
	if(mutex == NULL) {
		// Mutex não inicializado (alocado)
		PRINT("slock: mutex nulo\n");
		return -1;
	}
	if(mutex->flag != true && mutex->flag != false) {
		// Flag em estado incosistente
		PRINT("slock: flag do mutex em estado inconsistente\n");
		return -1;
	}
	
	contextSave(thisThread);
	
	// Quando essa thread sair da fila de espera do lock, for
	// desbloqueda e entrar numa das filas de STATE_READY, a
	// sua execução vai continuar a partir daqui. Ela vai ficar
	// num loop até conseguir entrar. Boa sorte.
	
	if(!mutex->flag) {
		mutex->flag = true; // Agora tem alguém no mutex!
		mutex->who  = thisThread; // Quem está no mutex?!
		PRINT("slock: tid %d CONSEGUIU entrar no lock\n", thisThread->tid);
	} else {
		// Já tem alguém no mutex. Bloqueia essa thread
		// e a coloca na fila de espera desse mutex.
		thisThread->state = STATE_BLOCKED;
		insert = queueAppend(mutex->lockedQueue, thisThread);
		if(!insert) {
			printf("slock: Erro fatal : não consegui inserir na fila de lockeds\n");
			exit(-1);
		}
		
		PRINT("slock: tid %d não conseguiu entrar no lock. Passa a vez...\n", thisThread->tid);
		
		// Segue o baile com outra thread
		dispatchThread(scheduler());
	}
	
	return 0;
}

int sunlock(smutex_t *mutex)
{
	s_tcb *thisThread, *unlocked;

	if(!isSThreadInitialized) {
		PRINT("sunlock: sthread não inicializada\n");
		return -1;
	}
	
	thisThread = getRunningThread();
	
	if(thisThread != mutex->who) {
		PRINT("sunlock: thread não pode desbloquer um lock que não é dona.\n");
		return -1;
	}
	
	PRINT("sunlock: tid %i saindo do lock\n", thisThread->tid);
	
	unlocked = queueRemove(mutex->lockedQueue);
	
	// Se tem alguém bloqueado nesse lock
	if(unlocked != NULL) {
		PRINT("sunlock: tid %d desbloqueada pela saída da tid %d\n", unlocked->tid, thisThread->tid);
		PRINT("sunlock: threads ainda bloqueadas neste lock:\n");
		queuePrint(mutex->lockedQueue, "lockedQueue");
		putInReadyState(unlocked);
	} else {
		// Liberando o mutex
		PRINT("sunlock: ninguém mais bloqueado no mutex. Lock está livre\n");
		mutex->flag = false;
	}
	
	return 0;
}
