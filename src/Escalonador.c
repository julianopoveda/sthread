#include <stdio.h>
#include <stdlib.h>
#include "../include/sdata.h"

fila_t *initFila() {
	fila_t *fila = (fila_t *) malloc(sizeof(fila_t));
	fila->first = NULL;
	fila->last  = NULL;
	return fila;
}

bool isFilaEmpty(fila_t *fila) {
	if(fila->first == NULL && fila->last == NULL) {
		return true;
	} else {
		return false;
	}
}

fila_t *filaPrioridadeAlta;//  = initFila();
fila_t *filaPrioridadeMedia;// = initFila();
fila_t *filaPrioridadeBaixa;// = initFila();
fila_t *filaBloqueados;// = initFila();

bool AdicionarFila(TCB *thread)
{
	fila_t *fila = NULL;
	//Cria um registro de elemento fila prioridade
	TCB *e = thread;
	
	printf("Entrando na adicionarFila... ");
	
	//Vê a qual lista de fila_t que o registro deve ser incluido
	fila = GetFila(thread);
	if(fila == NULL) {
		printf("Erro critico.\n");
		return false;
	} else if(isFilaEmpty(fila)) {
		fila->first = e;
		fila->last  = e;
		printf("Fila vazia.\n");
	} else {
		fila->last->next = e;
		fila->last = e;
		printf("Fila nao vazia.\n");
	}
	
	return true;
}
//Add no .h
//Ao chamar essa função o estado da thread não deve ter sido alterado ainda
bool RemoverFila(TCB *thread)
{
	fila_t *fila = NULL;
	//Cria um registro de elemento fila prioridade
	TCB *e = thread;
	
	//Vê a qual lista de fila_t que o registro deve ser incluido
	fila = GetFila(thread);
	if(fila == NULL) {
		printf("Erro critico.\n");
		return false;
	} else if(isFilaEmpty(fila)) {//se a fila estiver vazia ocorreu um erro também
		printf("Fila vazia.\n");
		return false;
	} else {
		TCB *threadAnterior = GetPenultimoElemento(fila);
		fila->last = threadAnterior;
		//Garante que não irá ocorrer erro caso a lista tornese vazia
		if(fila->last != NULL)
		{
			fila->last->next = NULL;
		}
	}
	
	return true;
}

//Add no .h
//Serve para obter o penúltimo elemento da fila
TCB *GetPenultimoElemento(fila_t *fila)
{
	TCB *threadPercorrer;
	
	if(filaPrioridadeAlta == NULL)
	{
		return NULL;
	}
	else
	{
		printf("%p", filaPrioridadeAlta->first);
		puts("nao e null");
		//Verifica se a fila não esta vazia
		if(filaPrioridadeAlta->first != NULL)
		{
			//Laço que busca o penúltimo elemento da lista. O laço não faz nada
			printf("%p", threadPercorrer);
			for (threadPercorrer = filaPrioridadeAlta->first; threadPercorrer->next != NULL; threadPercorrer = threadPercorrer->next)
			{
			}
			return threadPercorrer;
		}
		else
		{
			puts("entrou no else");
			return NULL;
		}
	}
}

/* BloquearTCB(TCB id)
{
	removerFilaAptos();
	adicionarFilaBloqueados();
} */

fila_t *GetFilaByThread(TCB *thread)
{
	//quem decide qual fila deve ser pega é o estado do programa
	switch (thread->estado)
	{
		case STATE_BLOCKED:
			return filaBloqueados;
		case STATE_READY:
			if(thread->prioridade == PRIO_HIGH) {
				return filaPrioridadeAlta;
			}
			else if(thread->prioridade == PRIO_MED) {
				return filaPrioridadeMedia;
			}
			else if(thread->prioridade == PRIO_LOW) {
				return filaPrioridadeBaixa;
			} else {
				return NULL;
			}
		default:
			return NULL;//se não houver estado definido dá erro
	}
}

void printFila(fila_t *fila, const char *nomeFila) {
	TCB *e;
	
	for(e = fila->first; e != NULL; e = e->next) {
		printf("%s: tid = %d\n", nomeFila, e->tid);
	}
}

int contador;

int screate (int prio, void (*start)(void*), void *arg)
{
	TCB *thread = (TCB *) malloc(sizeof(TCB));
	thread->prioridade = prio; // precisa verificar
	//thread->estado = STATE_READY;
	thread->tid = contador++;
	thread->next = NULL;
	
	printf("criada thread %d.\n", thread->tid);
	
	return AdicionarFila(thread);
}