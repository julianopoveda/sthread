/*
 * Programa para teste dos locks e mutexes.
 */

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#include <stdio.h>

#include "../include/sthread.h"
#include "../include/debug.h"

int global;

#define MAX 10

smutex_t mutex;

void func1(void *arg1)
{
	printf("func1: tentando entrar no lock...\n");
	slock(&mutex);
	
	  printf("func1: entrei no lock\n");
	  global++;
	  printf("func1: global %d\n", global);
	  syield();
	  printf("func1: voltei do yield\n");
	  
	sunlock(&mutex);
	
	printf("func1: acabei de sair do lock\n");
}

void func2(void *arg2)
{
	printf("func2: tentando entrar no lock...\n");
	slock(&mutex);
	
	  printf("func2: entrei no lock\n");
	  global++;
	  printf("func2: global %d\n", global);
	  syield();
	  printf("func2: voltei do yield\n");
	
	sunlock(&mutex);
	
	printf("func2: acabei de sair do lock\n");
}

int main()
{
	int t1, t2;
	
	turnDebugOn();
	smutex_init(&mutex);
	global = 0;
	
	t1 = screate(0, func1, NULL);
	t2 = screate(0, func2, NULL);
	
	
	swait(t1);
	swait(t2);
	

	return 0;
}
