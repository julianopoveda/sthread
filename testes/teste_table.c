/*
 * Programa para teste da tabela .
 * Para compilar: gcc table.c queue.c scheduler.c teste_table.c
 */

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>

#include "queue.h"
#include "scheduler.h"
#include "table.h"

#include "../include/sdata.h"


/*
 * Rotina para teste que simula a criação da thread.
 * Não usei a screate aqui pq as threads são referênciadas
 * pelo usuário por meio de um int (tid). E isso vai ser um problema.
 * Vamos precisar criar uma outras estrutura de dados para fazer esse
 * mapeamente para as filas de prioridades e a(s?) fila(s?) de bloquedados.
 */
s_tcb *threadCreate(int prio)
{
	s_queue *queue;
	s_tcb *thread;
	
	//////////////
	initSThread();
	//////////////
	
	switch(prio) {
		case PRIO_HIGH:
			queue = highPrioQueue;
			break;
		case PRIO_MED:
			queue = mediumPrioQueue;
			break;
		case PRIO_LOW:
			queue = lowPrioQueue;
			break;
		default:
			printf("Erro fatal: prioridade inválida ao criar thread.\n");
			exit(-1);
	}
	
	thread = (s_tcb *) malloc(sizeof(s_tcb));
	thread->prio = prio;
	thread->state = STATE_READY;
	thread->tid = genTid();
	thread->next = NULL;
	//printf("threadCreate: Alocada thread com tid %d.\n", thread->tid);
	
	queueAppend(queue, thread);
	
	return thread;
}

int main()
{
	s_tcb *ta, *tb, *tc, *td;
	s_tcb *aux;
	
	ta = threadCreate(0);
	tb = threadCreate(1);
	tc = threadCreate(2);
	td = threadCreate(0);
	
	threadTable = tableAlloc(1);
	
	tablePrint();
	tableAdd(ta->tid, ta);
	tablePrint();
	tableAdd(tb->tid, tb);
	tablePrint();
	tableAdd(tc->tid, tc);
	tablePrint();
	tableAdd(td->tid, td);
	
	tablePrint();
	
	aux = tableGet(2);
	printf("%d %d %d\n", aux->tid, aux->state, aux->prio);
	
	tablePrint();
	
	aux = tableRemove(ta);
	if(aux == NULL) printf("Não removido. thread não existe na tabela.\n");
	printf("%d %d %d\n", aux->tid, aux->state, aux->prio);
	
	tablePrint();
	
	aux = tableGet(0); // Já foi removida. Não pode retornar certo
	if(aux == NULL) { printf("tid não existe na table\n"); exit(-1); }
	printf("%d %d %d\n", aux->tid, aux->state, aux->prio);
	
	ta = threadCreate(0);
	tb = threadCreate(1);
	tc = threadCreate(2);
	td = threadCreate(0);

	puts("fock the police");

	return 0;
}
