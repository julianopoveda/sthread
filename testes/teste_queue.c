/*
 * Programa para teste do TAD das filas.
 * Algumas coisas, talvez mais do que necessário, são artificiais.
 * Esteja avisado.
 * 
 * Para compilar: gcc queue.c scheduler.c teste_queue.c
 * 
 * OBS: Esse teste vai funcionar possivelmente apenas nesse commit
 * porque estou usando uma função 'threadCreate()' para testes.
 */

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>

#include "queue.h"
#include "scheduler.h"
#include "global.h"

/*
 * Rotina para teste que simula a criação da thread.
 * Não usei a screate aqui pq as threads são referênciadas
 * pelo usuário por meio de um int (tid). E isso vai ser um problema.
 * Vamos precisar criar uma outras estrutura de dados para fazer esse
 * mapeamente para as filas de prioridades e a(s?) fila(s?) de bloquedados.
 */
s_tcb *threadCreateTest(int prio)
{
	s_queue *queue;
	s_tcb *thread;
	
	switch(prio) {
		case PRIO_HIGH:
			queue = highPrioQueue;
			break;
		case PRIO_MED:
			queue = mediumPrioQueue;
			break;
		case PRIO_LOW:
			queue = lowPrioQueue;
			break;
		default:
			printf("Erro fatal: prioridade inválida ao criar thread.\n");
			exit(-1);
	}
	
	thread = (s_tcb *) malloc(sizeof(s_tcb));
	thread->prio = prio;
	thread->state = STATE_READY;
	thread->tid = genTid();
	thread->next = NULL;
	printf("threadCreate: Alocada thread com tid %d.\n", thread->tid);
	
	queueAppend(queue, thread);
	
	return thread;
}

int main()
{
	s_tcb *ta, *tb, *tc, *td, *te;
	s_tcb *aux;
	
	initSThread();

	                          printf("main: Criando primeira thread.\n");
	ta = threadCreateTest(0); printf("\nmain: Criando segunda thread.\n");
	tb = threadCreateTest(0); printf("\nmain: Criando terceira thread.\n");
	tc = threadCreateTest(0); printf("\nmain: Criando quarta thread.\n");
	td = threadCreateTest(0);
	
	queuePrint(highPrioQueue, "Fila alta");
	printf("Após queuePrint()\n");
	
	aux = queueRemove(highPrioQueue);
	printf("main: Removed thread %d\n", aux->tid);
	aux = queueRemove(highPrioQueue);
	printf("main: Removed thread %d\n", aux->tid);
	aux = queueRemove(highPrioQueue);
	printf("main: Removed thread %d\n\n", aux->tid);
	
	queuePrint(highPrioQueue, "Fila alta");
	
	aux = threadCreateTest(0);
	aux = threadCreateTest(0);
	
	queuePrint(highPrioQueue, "Fila alta");
	
	aux = queueRemove(highPrioQueue); printf("%p\n", aux);
	aux = queueRemove(highPrioQueue); printf("%p\n", aux);
	aux = queueRemove(highPrioQueue); printf("%p\n", aux);
	aux = queueRemove(highPrioQueue); printf("%p\n", aux); // retorna NULL
	
	printf("aux deve ser NULL e é %p\n", aux);
	
	printf("Trabalhando com mais de uma fila\n\n");
	
	printf("main: Criando um monte de threads.\n");
	threadCreateTest(0);
	threadCreateTest(1);
	threadCreateTest(2);
	threadCreateTest(0);
	threadCreateTest(1);
	te = threadCreateTest(2);
	threadCreateTest(0);
	threadCreateTest(1);
	threadCreateTest(1); // repetido aqui
	ta = threadCreateTest(2);
	tb = threadCreateTest(0);
	tc = threadCreateTest(1);
	td = threadCreateTest(2);

	queuePrint(highPrioQueue, "Fila alta");
	queuePrint(mediumPrioQueue, "Fila média");
	queuePrint(lowPrioQueue, "Fila baixa");
	
	printf("Removendo...\n\n");
	
	aux = queueRemove(highPrioQueue);   printf("%p\n", aux);
	aux = queueRemove(mediumPrioQueue); printf("%p\n", aux);
	aux = queueRemove(mediumPrioQueue); printf("%p\n", aux); // repetido aqui
	aux = queueRemove(lowPrioQueue);    printf("%p\n", aux);
	
	queuePrint(highPrioQueue, "Fila alta");
	queuePrint(mediumPrioQueue, "Fila média");
	queuePrint(lowPrioQueue, "Fila baixa");
	
	bool retorno;
	
	// ta está na fila baixa e não está na ponta
	printf("tid da ta: %d\n", ta->tid);
	retorno = queueRemoveAsList(lowPrioQueue, ta);
	printf("Testando última função. Retorno: %d\n", retorno);
	queuePrint(lowPrioQueue, "low queue");
	
	printf("tid da td: %d\n", td->tid);
	retorno = queueRemoveAsList(lowPrioQueue, td);
	printf("Testando última função. Retorno: %d\n", retorno);
	queuePrint(lowPrioQueue, "low queue");
	queuePrintReverse(lowPrioQueue, "low queue");
	
	queuePrintReverse(highPrioQueue, "high queue reverse");
	
	printf("tid da lowPrioQueue->first: %d\n", lowPrioQueue->first->tid);
	retorno = queueRemoveAsList(lowPrioQueue, lowPrioQueue->first);
	printf("Testando última função. Retorno: %d\n", retorno);
	queuePrint(lowPrioQueue, "low queue");
	
	printf("tid da te: %d\n", te->tid);
	retorno = queueRemoveAsList(lowPrioQueue, te);
	printf("Testando última função. Retorno: %d\n", retorno);
	queuePrint(lowPrioQueue, "low queue");
	queuePrintReverse(lowPrioQueue, "low queue");
	
	ta = threadCreateTest(2);
	tb = threadCreateTest(2);
	tc = threadCreateTest(2);
	td = threadCreateTest(2);
	te = threadCreateTest(2);
	
	printf("\nnovo teste com a low queue\n\n");
	
	queuePrint(lowPrioQueue, "low queue");
	queuePrintReverse(lowPrioQueue, "low reverse");
	
	retorno = queueRemoveAsList(lowPrioQueue, tb);
	printf("Removendo tb tid: %d. Retorno: %d\n\n", tb->tid, retorno);
	
	queuePrint(lowPrioQueue, "low queue");
	queuePrintReverse(lowPrioQueue, "low reverse");
	
	retorno = queueRemoveAsList(lowPrioQueue, ta);
	printf("Removendo ta tid: %d. Retorno: %d\n\n", ta->tid, retorno);
	
	queuePrint(lowPrioQueue, "low queue");
	queuePrintReverse(lowPrioQueue, "low reverse");
	
	retorno = queueRemoveAsList(lowPrioQueue, te);
	printf("Removendo te tid: %d. Retorno: %d\n\n", te->tid, retorno);
	
	queuePrint(lowPrioQueue, "low queue");
	queuePrintReverse(lowPrioQueue, "low reverse");
	
	retorno = queueRemoveAsList(lowPrioQueue, tc);
	printf("Removendo tc tid: %d. Retorno: %d\n\n", tc->tid, retorno);
	
	queuePrint(lowPrioQueue, "low queue");
	queuePrintReverse(lowPrioQueue, "low reverse");
	
	retorno = queueRemoveAsList(lowPrioQueue, td);
	printf("Removendo td tid: %d. Retorno: %d\n\n", td->tid, retorno);
	
	queuePrint(lowPrioQueue, "low queue");
	queuePrintReverse(lowPrioQueue, "low reverse");
	
	retorno = queueRemoveAsList(lowPrioQueue, tc);
	printf("Removendo tc tid: %d. Retorno: %d\n\n", tc->tid, retorno);
	
	queuePrint(lowPrioQueue, "low queue");
	queuePrintReverse(lowPrioQueue, "low reverse");
	

	return 0;
}
