/*
 * Programa para teste do scheduler, contextos e thread main().
 * Ainda não implementei a screate(), mas devo conseguir inicializar
 * a biblioteca, criar o contexto da main(), e usar o escalonador para
 * selecionar a thread main().
 * Para compilar: gcc table.c queue.c context.c scheduler.c sthread.c teste_context.c
 */

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

// O usuário só incluí um header
//#include "../include/sthread.h"

ucontext_t threadDestroyContext;
ucontext_t final;
ucontext_t mainContext;

void threadDestroy()
{
	printf("E aqui é a threadDestroy() executando\n");
}

void destroy()
{
	printf("function destroy() executando\n");
}

void init()
{
	char *stack;
	getcontext(&threadDestroyContext);
	stack = (char *) malloc(SIGSTKSZ);
	threadDestroyContext.uc_link = &final;
	threadDestroyContext.uc_stack.ss_sp   = stack;
	threadDestroyContext.uc_stack.ss_size = SIGSTKSZ;
	makecontext(&threadDestroyContext, (void (*)(void))threadDestroy, 0);
}

int main()
{
	init();
	
	getcontext(&final);
	final.uc_link = &mainContext;
	final.uc_stack.ss_sp   = (char *) malloc(SIGSTKSZ);
	final.uc_stack.ss_size = SIGSTKSZ;
	makecontext(&final, (void (*)(void))destroy, 0);
	
	//int tid = screate(1, funcao, NULL);
	
	printf("main executando...\n");
	
	int flag = 0;
	getcontext(&mainContext);
	
	if(flag == 0) {
		flag = 1;
		setcontext(&threadDestroyContext);
	}
	
	//swait(tid);
	
	printf("Cheiguei até aqui. Sou um campeão!\n");

	return 0;
}
