/*
 * Testa wait de no máximo uma thread por thread.
 * Previne deadlock de forma simples.
 */

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#include <stdio.h>

#include "../include/sthread.h"
#include "../include/debug.h"

int tid1;
int tid2;

void func1(void *arg1)
{
	printf("Olá, sou a thread %d executando a func1\n", tid1);
	printf("Vou esperar a tid %d\n", tid2);
	swait(tid2);
}

void func2(void *arg2)
{
	int retorno;
	
	printf("Olá, sou a thread %d executando a func2\n", tid2);
	printf("Vou esperar a tid %d\n", tid1);
	retorno = swait(tid1);
	
	if(retorno == -1) {
		printf("Não consegui esperar a tid %d.\n", tid2);
		printf("Ela já deve estar sendo esperada por outra thread...\n");
	} else {
		printf("Blz. Consegui esperar a thread. Mas isso é ruim. Aconteceu mesmo?\n");
	}
	
}

int main()
{
	turnDebugOn();
	
	tid1 = screate(0, func1, NULL);
	tid2 = screate(0, func2, NULL);
	
	swait(tid1);
	
	printf("Sucesso. Deadlock não aconteceu!\n");
	
	return 0;
}
