/*
 * Este programa testa a inicialização da sthread.
 */

/*
 * UFRGS : INF01142 : Prof. Alexandre Carissimi
 * Biblioteca de threads em nível de usuário
 * Autores: César Pastorini e Juliano Poveda
 */

#include <stdio.h>

#include "../include/sthread.h"
#include "../include/debug.h"

smutex_t mutex;

void f_thread(void *arg)
{
	printf("Olá mundo. Sou a thread executando.\n");
}

int main()
{
	int tid1;
	
	turnDebugOn();
	
	smutex_init(&mutex);
	
	printf("Códigos retornados:\n");
	
	printf("swait(12): %d\n", swait(12));
	printf("syield(): %d\n", syield());
	printf("slock(&mutex): %d\n", slock(&mutex));
	printf("sunlock(&mutex): %d\n", sunlock(&mutex));
	
	printf("E finalmente inicializando...\n");
	
	printf("tid1 = screate(1, f_thread, NULL);\n");
	tid1 = screate(1, f_thread, NULL);
	printf("tid1 : %d\n", tid1);
	
	swait(tid1);

	return 0;
}
