/*
 * scheduler.h
 */

#ifndef __SCHED_H__
#define __SCHED_H__

#include "../include/sdata.h"

#include "queue.h"

/*
 * Estas variáveis globais estão declaradas em scheduler.c
 */

// O escalonador usa três filas de prioridades
extern s_queue *highPrioQueue;
extern s_queue *mediumPrioQueue;
extern s_queue *lowPrioQueue;
extern s_queue *blockedQueue;
extern s_queue *runningQueue;

//extern s_tcb *runningThread;

s_queue *getReadyQueue(s_priority prio);
s_tcb *scheduler();

#endif // __SCHED_H__