/*
 * queue.h
 * Arquivo de cabeçalho para o TAD das filas.
 * É um arquivo de cabeçalho privado, por isso não
 * está no diretório 'include'.
 */

#ifndef __S_QUEUE__
#define __S_QUEUE__

#include "../include/sdata.h"

s_queue *queueAlloc();
s_tcb *queueRemove(s_queue *queue);
bool queueRemoveAsList(s_queue *queue, s_tcb *trd);
bool queueIsEmpty(s_queue *queue);
bool queueAppend(s_queue *queue, s_tcb *thread);
void queuePrint(s_queue *queue, const char *nomeFila);
void queuePrintReverse(s_queue *queue, const char *nomeFila);


#endif //__S_QUEUE__