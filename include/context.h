/*
 * context.h
 */

#ifndef __S_CONTEXT_H__
#define __S_CONTEXT_H__

#include <ucontext.h>

#include "../include/sdata.h"

extern s_tcb *runningThread;
extern s_tcb *mainThread;

s_tcb *getRunningThread();
void contextSave(s_tcb *thread);
void dispatchThread(s_tcb *thread);
void putInReadyState(s_tcb *thread);
//void putInBlockedState(s_tcb *thread);

#endif //__S_CONTEXT_H__
