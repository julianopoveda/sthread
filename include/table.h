/*
 * table.h
 * Implementa a table que mapeia tid para ponteiros s_tcb.
 */

#ifndef __S_TABLE__
#define __S_TABLE__

#include "../include/sdata.h"

typedef struct {
	int tid;
	s_tcb *thread;
	bool isValid; // se linha contém valor válido
} s_table_row;

typedef struct {
	s_table_row *row;
	int size;
} s_table;

extern s_table *threadTable;

s_table *tableAlloc(int size);
s_tcb *tableGet(int tid);
bool tableAdd(int tid, s_tcb *thread);
s_tcb *tableRemove(s_tcb *thread);
//s_tcb tableRemove(int tid); // por enquanto não vai precisar
void tablePrint();

#endif //__S_TABLE__
