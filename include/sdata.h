/*
 * sdata.h: arquivo de inclusão onde os grupos devem colocar as definições
 *          de suas estruturas de dados
 *
 * VERSÃO 1 - 20/08/2014
 */

#ifndef __SDATA_H__
#define __SDATA_H__

#include <ucontext.h>

// Não podemos programar sem isso
typedef enum {
	true  = 1, 
	false = 0
} bool;

typedef enum {
	PRIO_HIGH = 0,
	PRIO_MED  = 1,
	PRIO_LOW  = 2
} s_priority;

typedef enum {
	STATE_NEW     = 0, // não precisa
	STATE_READY   = 1,
	STATE_BLOCKED = 2,
	STATE_RUNNING = 3,
	STATE_UNDEF   = 4
} s_state;

<<<<<<< HEAD
 /*
  * Exemplo de estrutura de dados da Thread Control Block
  */
 typedef struct tcb {
	state estado;
=======
typedef struct tcb {
>>>>>>> 304821bb38051d2b4d2cdaad290a60f620e0a90d
	int tid;
	s_state state;
	s_priority prio;
	ucontext_t context;
	struct tcb *waitedBy;
	struct tcb *next;
	struct tcb *prev;
} s_tcb;

typedef struct {
	s_tcb *first;
	s_tcb *last;
} s_queue;

typedef struct mutex {
	int flag;
    TCB *first;
	TCB *last;
} mmutex_t;


extern fila_t *filaPrioridadeAlta;//  = initFila();
extern fila_t *filaPrioridadeMedia;// = initFila();
extern fila_t *filaPrioridadeBaixa;// = initFila();
extern fila_t *filaBloqueados;// = initFila();
extern int contador;


/* Funções */

 fila_t *initFila();
 bool isFilaEmpty(fila_t *fila);
 bool AdicionarFila(TCB *thread);
 fila_t *GetFila(TCB *thread);
 void printFila(fila_t *fila, const char *nomeFila);
 int screate (int prio, void (*start)(void*), void *arg);
 TCB *GetPenultimoElemento(fila_t *fila);
 bool RemoverFila(TCB *thread);
