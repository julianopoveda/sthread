/*
 * scheduler.h
 */

#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include "../include/sdata.h"

int genTid();
void initSThread();
s_tcb *threadCreate(int prio);
void threadDestroy();
void continueSThread();
char *threadAsString(s_tcb *thread);

#endif // __GLOBAL_H__