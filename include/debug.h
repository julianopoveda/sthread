// debug.h

/*
 * UFRGS : INF01147 : Turma B : Prof. Marcelo Johann
 * Etapa 1 do trabalho: analisador léxico.
 * Mais info em http://inf.ufrgs.br/~johann/comp/spect1.pdf
 * Autores: Daniela Cavalheiro e César Pastorini
 */

#ifndef	__DEBUG__
#define __DEBUG__

#include <stdarg.h>
#include "sdata.h"

// Wrapper da printf.
void PRINT(const char* format, ...);
void turnDebugOn();
void turnDebugOff();
bool isDebugOn();


#endif //__DEBUG__
